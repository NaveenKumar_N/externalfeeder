package com.gap.feed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExternalFeederApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExternalFeederApplication.class, args);
	}

}
